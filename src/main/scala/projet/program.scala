package projet

import java.io.{BufferedWriter, File, FileInputStream, FileWriter}
import projet.models._
import java.text.SimpleDateFormat
import net.liftweb.json._
import scala.io.Source


object Program {

  def main(args: Array[String]): Unit = {
    var catseq = Seq.empty[Cat];
    var actorseq = Seq.empty[Actor];
    var carseq = Seq.empty[Car];
    var movieseq = Seq.empty[Movie];
    var personseq = Seq.empty[Person];
    val format = new SimpleDateFormat("dd/MM/yyyy");
    val filename = "Dataset.csv";


    //  val line = 4;   //choisir une ligne a tester


    //////////////////// Creation JSON /////////////////////////
    /*
    val test = ReadFile(filename);
    for (i<-0 to test.length-1) {
      val res = Split(test(i));
      val idClass = Matching(res);
      idClass match {
        case 1 => actorseq = actorseq :+ Actor(res(0), SplitList(res(1)).toSeq)
        case 2 => carseq = carseq :+ Car(res(0),res(1),Integer.valueOf(res(2)),Integer.valueOf(res(3)))
        case 3 => catseq = catseq :+ Cat(res(0),res(1),res(2).toInt)
        case 4 => movieseq = movieseq :+ Movie(SplitList(res(0)).toSeq,format.parse(res(1)))
        case 5 => personseq = personseq :+ Person(res(0),res(1),Integer.valueOf(res(2)),Integer.valueOf(res(3)))
        case 6 => personseq = personseq :+ Person(res(0),res(1),Integer.valueOf(res(2)),0)
      }

    }
    val file = new File("Dataset.json");
    val bw = new BufferedWriter(new FileWriter(file));
    val text = appendedText(actorseq,carseq,catseq,movieseq,personseq);
    //println(text)
    bw.write(text)
    bw.close()
    */

    ///////////////////////////////////////////////////////////////



    ////////////////// Creation CSV From JSON //////////////////////////

    val filename2 = ReadFile("Dataset.json").mkString;

    implicit val formats = DefaultFormats
    val json = parse(filename2)
    val CatJson = (json \ "Cat").extract[List[Cat]]
    val ActorJson = (json \ "Actor").extract[List[Actor]]
    val CarJson = (json \ "Car").extract[List[Car]]
    //val MovieJson = (json \ "Movie").extract[List[Movie]]
    val PersonJson = (json \ "Person").extract[List[Person]]




    val file = new File("CSV.csv");
    val bw = new BufferedWriter(new FileWriter(file));
    val csv = appendCsv(appendActorJson(ActorJson),appendCatJson(CatJson),appendCarJson(CarJson),appendPersonJson(PersonJson))
    println(csv)
    bw.write(csv)
    bw.close()

    /////////////////////////////////////////////////////////


  }


    //*************CHOISIR UNE LIGNE************//
    /*
    val res = Split(test(line-1));
    val idClass = Matching(res);
    idClass match {
      case 1 => println("it's an Actor")
      case 2 => println("its a Car")
      case 3 => println("it's a Cat")
      case 4 => println("it's a Movie")
      case 5 => println("it's a Person")
    }
    }
    */
    ///////////////////////////////////////////

  ////////////////////////////////////////////////
  /*
  def ChooseLine(entryTab: String): Array[String]={
     return entryTab.split(", ");
  }
  //////////////////////////////////////////////////
  */


  def ReadFile(file: String): Array[String] ={
    return Source.fromFile(file).getLines.toArray;
  }

  def Split(tab: String): Array[String] ={
    return tab.split(", ")
  }

  def SplitList(tab: String): Array[String] ={
    return tab.split(";")
  }

  def Matching(res: Array[String]): Int = {
    res.length match {

      case 3 => CatOrPerson(res)
      case 4  => CarOrPerson(res)
      case 2  => ActorOrMovie(res)
      case _ => 0
    }
  }

  def CatOrPerson(res: Array[String]): Int = {
    if(res(0).head.isUpper){
      6
    }else{
      3
    }
  }

  def CarOrPerson(res: Array[String]): Int = {
    if(Integer.valueOf(res(2)) > 500){
      5
    }else{
      2
    }
  }

  def ActorOrMovie(res: Array[String]): Int = {
    val seq1 = SplitList(res(0))
    if( seq1.length > 1){
      4
    }else{
      1
    }
  }


  def appendedText(actorseq: Seq[Actor],carseq: Seq[Car],catseq: Seq[Cat],movieseq: Seq[Movie],personseq: Seq[Person]): String = {
    val text = appendActor(actorseq,"Actor") + appendCar(carseq,"Car") + appendCat(catseq,"Cat") + appendMovie(movieseq,"Movie") + appendPerson(personseq,"Person")

    return "{\n" + text + "\n}"
  }

  def appendCat(sequence: Seq[Cat], className: String): String = {
    var text = "\"" + className + "\":\n["
    for(i<-0 to sequence.length-1){
      text += "\n{\"name\" : \""
      text += sequence(i).name
      text += "\","
      text += "\"race\" : \""
      text += sequence(i).race
      text += "\","
      text += "\"age\" : "
      text += sequence(i).age
      text += "}"
      if (i < sequence.length-1){
        text += ","
      }
    }
    text += "\n],\n"
    return text
  }

  def appendActor(sequence: Seq[Actor], className: String): String = {
    var text = "\"" + className + "\":\n["
    for(i<-0 to sequence.length-1){
      text += "\n{\"name\" : \""
      text += sequence(i).name
      text += "\","
      text += "\"filmsPlayed\" : "
      text += appendSubSeq(sequence(i).filmsPlayed)
      text += "}"
      if (i < sequence.length-1){
        text += ","
      }
    }
    text += "\n],\n"
    return text
  }

  def appendCar(sequence: Seq[Car], className: String): String = {
    var text = "\"" + className + "\":\n["
    for(i<-0 to sequence.length-1){
      text += "\n{\"brand\" : \""
      text += sequence(i).brand
      text += "\","
      text += "\"countryOfBirth\" : \""
      text += sequence(i).countryOfBirth
      text += "\","
      text += "\"maxSpeed\" : "
      text += sequence(i).maxSpeed
      text += ","
      text += "\"speeds\" : "
      text += sequence(i).speeds
      text += "}"
      if (i < sequence.length-1){
        text += ","
      }
    }
    text += "\n],\n"
    return text
  }

  def appendMovie(sequence: Seq[Movie], className: String): String = {
    var text = "\"" + className + "\":\n["
    for(i<-0 to sequence.length-1){
      text += "\n{\"mainActors\" : "
      text += appendSubSeq(sequence(i).mainActors)
      text += ","
      text += "\"dateOfRelease\" : \""
      text += sequence(i).dateOfRelease
      text += "\"}"
      if (i < sequence.length-1){
        text += ","
      }
    }
    text += "\n],\n"
    return text
  }

  def appendPerson(sequence: Seq[Person], className: String): String = {
    var text = "\"" + className + "\":\n["
    for(i<-0 to sequence.length-1){
      text += "\n{\"firstName\" : \""
      text += sequence(i).firstName
      text += "\","
      text += "\"lastName\" : \""
      text += sequence(i).lastName
      text += "\","
      text += "\"salary\" : "
      text += sequence(i).salary
      text += ","
      text += "\"numberOfChildren\" : "
      text += sequence(i).numberOfChildren
      text += "}"
      if (i < sequence.length-1){
        text += ","
      }
    }
    text += "\n]"
    return text
  }

  def appendSubSeq(sequence: Seq[String]): String = {
    var text = "["
    for (i <- 0 to sequence.length-1) {
      text += "\"" + sequence(i) + "\""
      if (i < sequence.length - 1) {
        text += ","
      }

    }
    text += "]"
    text
  }

  def appendCarJson(cars: List[Car]): String ={
    var res: String = ""
    for (i <- 0 to cars.length-1){
      res += (""+cars(i).brand+", "+cars(i).countryOfBirth+", "+cars(i).maxSpeed+", "+cars(i).speeds+"\n")
    }
    return res
  }

  def appendCatJson(cats: List[Cat]): String ={
    var res: String = ""
    for (i <- 0 to cats.length-1){
      res += (""+cats(i).name+", "+cats(i).race+", "+cats(i).age+"\n")
    }
    return res
  }

  def appendPersonJson(persons: List[Person]): String ={
    var res: String = ""
    for (i <- 0 to persons.length-1){
      res += (""+persons(i).firstName+", "+persons(i).lastName+", "+persons(i).salary+", "+persons(i).numberOfChildren+"\n")
    }
    return res
  }

  def appendActorJson(actors: List[Actor]): String ={
    var res: String = ""
    for (i <- 0 to actors.length-1){
      res += actors(i).name+", "+actors(i).filmsPlayed.mkString(";")+"\n"
    }
    return res
  }

  def SplitFilms(films: Seq[String]): String = {
    var res = ""
    for (i <- 0 to films.length-1){
      res += films(i)+";"
    }
    return res
  }

  def appendCsv(actors: String,cars: String, cats: String, persons: String): String = {
    val res: String  = actors + cars + cats + persons

    return res
  }


}

