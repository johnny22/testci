name := "Projet_windows"

version := "0.1"

scalaVersion := "2.11.11"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.2.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0"
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.2.0"

publishTo := {
  // URL used when running job in Jenkins
  val nexusRepositoriesUrl = "http://localhost:8081/nexus/content/repositories"

  if (isSnapshot.value)
    Some("Snapshots" at s"$nexusRepositoriesUrl/snapshots")
  else
    Some("Releases" at s"$nexusRepositoriesUrl/releases")
}